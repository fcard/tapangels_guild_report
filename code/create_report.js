(() => {
'use strict';

  window.cancel_get_members = false;
  window.GuildProgress = {};

  var byClass = (c) => [...document.body.getElementsByClassName(c)]
  var textByClass = (c) => byClass(c).map((x)=>x.innerText);

  GuildProgress.version = "0.1.24";

  GuildProgress.get_members = function(callback) {
    function with_members() {
      var nameslv = textByClass("gmname").slice(1);
      var levels  = textByClass("gmlevel").slice(1);
      var raiddmg = textByClass("gmraiddmg").slice(1);
      var raidnum = textByClass("gmraidatt").slice(1);

      var names = nameslv.map((x)=>/(.*) Lv.*/.exec(x)[1]);

      var guildName = textByClass("guildname")[0];
      var guildSize = parseInt(/.*Member: ([0-9]+) \/.*/.exec(guildName)[1]);

      var members = {};
      for (var i = 0; i < guildSize; i+=1) {
        if (names[i]) {
          members[names[i]] = {
            'level': parseInt(levels[i]),
            'raid_damage': parseInt(raiddmg[i]),
            'raid_attacks': parseInt(raidnum[i])
          };
        } else {
          throw "Unable to find all members!";
        }
      }
      callback(members);
    }

    function wait_for_members() {
      gmemberanz();
      if (byClass("gmname").length > 0) {
        setTimeout((()=>{
          if (byClass("gmname").length > 0) {
            with_members();
          } else {
            wait_for_members();
          }
        }), 1000);
      } else if (!cancel_get_members) {
        setTimeout(wait_for_members, 1000);
      } else {
        cancel_get_members = false;
      }
    }

    guild();
    wait_for_members();
  }

  GuildProgress.create_document = function(data, last_members, members) {
    var progress    = [];
    var no_progress = [];
    var new_members = [];

    for (var member_name in members) {
      var report = member_name;
      var member = members[member_name];

      if (last_members.hasOwnProperty(member_name)) {
        var member_last = last_members[member_name];
        var did_thing = false;
        if (member_last.level < member.level) {
          report += ` has gained ${member.level - member_last.level} levels`;
          data.progress_count[member_name] += member.level - member_last.level;
          did_thing = true;
        }
        if (member_last.raid_attacks < member.raid_attacks) {
          if (did_thing) {
            report += " and";
          }
          report += ` has attacked ${member.raid_attacks - member_last.raid_attacks} times`;
          data.progress_count[member_name] += member.raid_attacks - member_last.raid_attacks;
          did_thing = true;
        }
        if (!did_thing) {
          report += ` did nothing. (${data.progress_count[member_name]})`
          no_progress.push([report, data.progress_count[member_name]]);
        } else {
          report += `. (${data.progress_count[member_name]})`;
          progress.push([report, data.progress_count[member_name]]);
        }
      } else {
        var new_member_progress = member.raid_attacks
        report += ` is a new member! (${new_member_progress})`;
        data.progress_count[member_name] = new_member_progress;
        new_members.push([report, data.progress_count[member_name]]);
      }
    }
    data.date = Date();

    var page = "<!DOCTYPE html>\n<html>\n<head><style> body { background-color: rgb(30,30,30); } </style>\n</head>\n\n<body>\n";

    no_progress = no_progress.map(  ([report,c]) => [`<p style="color: red;">${report}</p>\n`,   c]);
    progress    = progress.map(     ([report,c]) => [`<p style="color: green;">${report}</p>\n`, c]);
    new_members = new_members.map(  ([report,c]) => [`<p style="color: yellow;">${report}</p>\n`,c]);

    var reports = no_progress.concat(progress, new_members).sort((x,y)=>y[1]-x[1]).map(x=>x[0]);

    page += '<div id="reports">\n';
    reports.forEach(report => page += report);
    page += "</div>\n</body>\n</html>\n";

    return page;
  }

  GuildProgress.download = function(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/html;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
      var event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
    }
    else {
      pom.click();
    }
  }

  GuildProgress.init = function () {
    localStorage.setItem("guild_data", `{"progress_count":{}, "date":"${Date()}"}`);
    localStorage.setItem("guild_members", "{}");
  }

  GuildProgress.obtain = function () {
    var file = document.createElement('input');
    file.setAttribute("type", "file");
    file.setAttribute("multiple", "true");
    file.oninput = function(e) {
      GuildProgress.readFiles(file.files);
    }
    file.click();
  }

  GuildProgress.readFiles = function (files) {
    if (files.length == 2) {
      if (/members/.test(files[0].name)) {
        var [guild_members, guild_data] = files;
      } else if (/data/.test(files[0].name)) {
        var [guild_data, guild_members] = files;
      } else {
        return;
      }
      var frd = new FileReader();
      var frm = new FileReader();
      frd.readAsText(guild_data);
      frm.readAsText(guild_members);

      frd.onload = (e) => localStorage.setItem("guild_data", frd.result);
      frm.onload = (e) => localStorage.setItem("guild_members", frm.result);
    }
  }


  GuildProgress.update = function () {
    var data = JSON.parse(localStorage.getItem("guild_data"));
    var last_members = JSON.parse(localStorage.getItem("guild_members"));

    GuildProgress.get_members(members => {

      var page = GuildProgress.create_document(data, last_members, members);

      GuildProgress.download(`${Date.now()}_report.html`,  page);
      GuildProgress.download(`${Date.now()}_data.json`,    JSON.stringify(data));
      GuildProgress.download(`${Date.now()}_members.json`, JSON.stringify(members));

      localStorage.setItem("guild_data", JSON.stringify(data));
      localStorage.setItem("guild_members", JSON.stringify(members));
    });
  }

  GuildProgress.reload = function () {
    var old_script = document.getElementById("guild-progress-script");
    if (old_script) {
      document.body.removeChild(old_script);
    }
    var script = document.createElement("script");
    script.setAttribute("type", "application/javascript");
    script.setAttribute("src", "https://gl.githack.com/fcard/tapangels_guild_report/raw/master/code/create_report.js");
    script.setAttribute("id", "guild-progress-script");
    document.body.appendChild(script);
  }

  GuildProgress.show = function () {
    window.open("https://www.dropbox.com/s/yebvvat9spej9xi/report.html?dl=0");
  }
})();
