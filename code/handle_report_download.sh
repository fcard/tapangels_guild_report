#!/bin/sh

Dir="$HOME/Dropbox/TapAngels WHY Guild Progress"
FileLocation="$1"
FileName="$(basename "$1")"

mv "$FileLocation" "$Dir/$FileName"

cp "$Dir/$FileName" "$Dir/history/$FileName"
mv -f "$Dir/$FileName" "$Dir/$(echo "$FileName" | sed -e 's/.*_\(.*\)/\1/')"

if echo "$FileName" | grep report; then
  (
    cd "$Dir"

    git add .
    git commit -m "update reports and data '$(date)'"
    git push -u origin HEAD
  )
fi
